//
//  Levels.swift
//  KickTheEatingDisorderOut
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//

import Foundation

struct LevelDetails {
    
    
    var isLocked : Bool
    var VideoName : String
    var backgrounImage: String
    
    
    
    init(isLocked:Bool,VideoName:String, backgrounImage:String) {
        
        
        self.isLocked = isLocked
        self.VideoName = VideoName
        self.backgrounImage = backgrounImage
        
    }
}
